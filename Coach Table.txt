-- Table: public.apptwo_coach

-- DROP TABLE public.apptwo_coach;

CREATE TABLE public.apptwo_coach
(
    id integer NOT NULL DEFAULT nextval('apptwo_coach_id_seq'::regclass),
    coach_id integer NOT NULL,
    first_name character varying(30) COLLATE pg_catalog."default" NOT NULL,
    last_name character varying(30) COLLATE pg_catalog."default" NOT NULL,
    email_address character varying(150) COLLATE pg_catalog."default" NOT NULL,
    phone_number character varying(15) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT apptwo_coach_pkey PRIMARY KEY (id),
    CONSTRAINT apptwo_coach_email_address_key UNIQUE (email_address)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.apptwo_coach
    OWNER to postgres;

-- Index: apptwo_coach_email_address_2a8112d3_like

-- DROP INDEX public.apptwo_coach_email_address_2a8112d3_like;

CREATE INDEX apptwo_coach_email_address_2a8112d3_like
    ON public.apptwo_coach USING btree
    (email_address COLLATE pg_catalog."default" varchar_pattern_ops)
    TABLESPACE pg_default;